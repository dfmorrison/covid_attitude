The file covid_attitude implements a simple cognitive model for the
effect of attitudes towards mask wearing on disease spread. It can
be imported as a Python module into a larger program, or run as a
stand alone example.

Note that this depends upon several other modules. To ensure they are
all installed simply do

pip install -r requirements.txt

If importing it the major entry point is the MaskWearer object. This takes
a variety of arguments for describing attitudes, epidemiological parameters,
descriptions of the initial population, and ACT-R memory parameters.
Its run_period() method is called advance its state by one time period;
this method returns a dict containing the revised rate of infection and
the updated state of the population.

As a standalone application it creates and runs for a designated number of
periods (100 by default) a collection of MaskWearers (100 by default) and
averages the results at each time step over this collection. The results
are then graphed and printed as a table. The various parameters for the
MaskWearers can be provided as command line options. To do this entirely
with the default arguments simply execute

python covid_attitude.py

To see the available command line options you can either examine the source
or run

python covid_attitude.py --help
